addEventListener('fetch', event => {
  event.respondWith(handleRequest(event.request))
})

async function handleRequest(request) {
  const response = await fetch('https://ianbland.com.au/category/bland-on-bland/feed/', request)
  const { status, statusText, headers } = response;
  const text = await response.text()

  return new Response(await updateRss(text), { status, statusText, headers })
}

async function updateRss(text) {
  const chunks = await Promise.all(
    text.split(/(<item>.*?<\/item>)/s)
      .map(chunk => chunk.startsWith('<item>') ? updateItem(chunk) : chunk)
      )
  return chunks.join('')
}

async function updateItem(text) {
  const [ , audioUrl ] = text.match(/<audio .*<a href="([^"]*)".*<\/a><\/audio>/s) || []
  const [ , enclosureUrl ] = text.match(/<enclosure url="([^"]*)".*?\/>/s) || []
  if (!audioUrl) {
    return text.replace(/<enclosure .*?\/>/, "")
  }
  else if (audioUrl !== enclosureUrl) {
    return text.replace(/<enclosure .*?\/>/, await enclosure(audioUrl))
  }
  return text;
}

async function enclosure(url) {
  const response = await fetch(url, { method: 'HEAD' })
  const length = response.headers.get('Content-Length')
  const type = response.headers.get('Content-Type')

  return `<enclosure url="${url}" length="${length}" type="${type}" />`
}

Bland on Bland RSS
==================

The RSS feed for [Bland on
Bland](https://ianbland.com.au/category/bland-on-bland/feed/) has
incorrect URLs specified in the `<enclosure>` tags.

I wanted to try [Cloudflare Workers](https://workers.cloudflare.com/),
so I put together a simple one to produce a version of the RSS feed with
the correct enclosure URLs.

I now have my podcast application using the modified RSS feed at
<https://bland-on-bland.pyrites.workers.dev>.
